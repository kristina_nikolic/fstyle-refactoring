<?php

namespace Drupal\fashion_style_block\plugin\block;

use Drupal\block\Entity\Block;
use Drupal\Core\Block\BlockBase;

use Drupal\views\Views;


/**
 * Provides a 'Fashion Style' block.
 *
 * @Block(
 *   id = "fashion_style_block",
 *   admin_label = @Translation("Fashion Style Block"),
 *   category = @Translation("Blocks"),
 * )
 */

class FashionStyleBlock extends BlockBase {

    public function build() {

        $view = Views::getView('product_overview');
        $block = Block::load('searchform');
        $build['search'] = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
        if($view){
            $view->setDisplay('block_1');
            $view->preExecute();
            $view->execute();
            $build['sugestion']  = $view->buildRenderable('block_1');
        }

        return $build;
    }

}