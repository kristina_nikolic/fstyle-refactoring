<?php

namespace Drupal\fashion_style_twig_helper;

use Drupal\file\Entity\File;

class FashionStyleTwigHelper extends \Twig_Extension {

    public function getName() {
        return 'fashion_style_twig_helper.twig_extension';
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('getImage', array($this, 'getImage'), array('is_safe' => array('html'))),
        ];
    }

    public function getImage($imageId) {
        $template = '';
        $image = File::load($imageId);
        $imageUrl = file_create_url($image->getFileUri());
//        if($key == 0) {
//            $template .= '<div class="img-wrap"><img class="full-width" src="' . $imageUrl . '" alt=""></a></div>';
//            $key ++;
//        } else {
            $template .= '<div class="thumb"><a href="assets/img/content/product-page-img-big-1.jpg"><img src="' . $imageUrl . '" alt=""></a></div>';
//        }
        return $template;
    }
}