<?php

namespace Drupal\fashion_style_form\service;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;

class CalculateService {

    protected $numbers = [2, 2];
    protected $currentUser;
    private $routeMatch;

    public function __construct(RouteMatchInterface $routeMatch, AccountProxyInterface $currentUser)
    {
        $this->currentUser = $currentUser;
        $this->routeMatch = $routeMatch;
    }

    public function calculate() {
        $result = $this->numbers[0] + $this->numbers[1];
        return '2+2=' . $result;
    }

    public function calculateIds() {
        return $this->currentUser->id() + $this->routeMatch->getParameter('node')->id();
    }
}