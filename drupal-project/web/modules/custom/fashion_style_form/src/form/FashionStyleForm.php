<?php

namespace Drupal\fashion_style_form\form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fashion_style_form\service\CalculateService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FashionStyleForm extends ConfigFormBase {

    protected $calculate;

    public function __construct(ConfigFactoryInterface $config_factory, CalculateService $calculate)
    {
        parent::__construct($config_factory);
        $this->calculate = $calculate;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
           $container->get('fashion_style.calculate')
        );
    }

    public function getFormId() {
        return 'fashion_style_form';
    }

    protected function getEditableConfigNames() {
        return ['fashion_style_form.settings'];
    }

    public function buildForm(array $form, FormStateInterface $form_state, $nodeId = NULL, $nodeTitle = NULL) {

        $form['calculate'] = [
            '#type' => 'markup',
            '#value' => $this->calculate->calculate(),
            '#markup' => $this->t('Your calculation: ' . $this->calculate->calculate()),
            ];

        $form['calculateIds'] = [
            '#type' => 'markup',
            '#value' => $this->calculate->calculateIds(),
            '#markup' => $this->t('UserId + NodeId = ' . $this->calculate->calculateIds()),
        ];


        $form['node_id'] = [
            '#type' => 'markup',
            '#value' => $nodeId,
            '#markup' => $this->t('Node Id:' . $nodeId),
        ];

        $form['node_title'] = [
            '#type' => 'markup',
            '#value' => $nodeTitle,
            '#markup' => $this->t('Node Title:' . $nodeTitle),
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Enter your email'),
        ];

        return parent::buildForm($form, $form_state);
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if($form_state->hasValue('email')) {
            $email = $form_state->getValue('email');
            $userEmail = \Drupal::currentUser()->getEmail();
            if(!filter_var($email, FILTER_VALIDATE_EMAIL) || ($userEmail !== $email)) {
                $form_state->setErrorByName('email', t('The email is invalid.'));
            }
        }
        parent::validateForm($form, $form_state);
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user = \Drupal::currentUser();
        $userName = $user->getAccountName();
        $email = $form_state -> getValue('email');
        $node = $form_state->getCompleteForm()['node_title']['#value'];
        $config = $this->config('fashion_style_form.settings');
        $config->set('name', $userName);
        $config->set('email', $email);
        $config->set('node', $node);
        $config->save();

        parent::submitForm($form, $form_state);
    }
}