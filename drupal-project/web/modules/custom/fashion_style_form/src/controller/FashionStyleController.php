<?php

namespace Drupal\fashion_style_form\controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;


class FashionStyleController extends ControllerBase {

    public function form(NodeInterface $node) {
        $nodeId = $node->id();
        $nodeTitle = $node->getTitle();

        $form = \Drupal::formBuilder()->getForm('\Drupal\fashion_style_form\form\FashionStyleForm', $nodeId, $nodeTitle);
        return $form;
    }
}