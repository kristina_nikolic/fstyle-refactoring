<?php

namespace Drupal\fashion_style_maps\Plugin\Block;

use Drupal\block\Entity\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Maps' block.
 *
 * @Block(
 *   id = "fashion_style_maps",
 *   admin_label = @Translation("Fashion Style Maps"),
 *   category = @Translation("Blocks"),
 * )
 */

class FashionStyleMapsBlock extends BlockBase {

    public function build()
    {
       return [
           'inside' => [
               '#type' => 'html_tag',
               '#tag' => 'div',
               '#attributes' => [
                   'id' => ['map'],
                   'height' => ['100%']
               ],
               '#attached' => [
                   'library' => ['fashion_style_maps/maps_script'],
               ],
           ]
       ];
    }
}