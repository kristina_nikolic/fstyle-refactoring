(function($, Drupal) {
    $(".views-field-filename").each(function() {
        $("a").each(function () {
            var NewElement = $("<img >");
            $.each(this.attributes, function(i, attrib) {
                $(NewElement).attr("src", attrib.value);
            })

            $(this).replaceWith(function () {
                return $(NewElement);
            });
        });
    });
})(jQuery, Drupal);