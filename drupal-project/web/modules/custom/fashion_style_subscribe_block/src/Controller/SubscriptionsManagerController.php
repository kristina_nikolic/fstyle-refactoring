<?php

namespace Drupal\fashion_style_subscribe_block\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\fashion_style_subscribe_block\Service\DatabaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SubscriptionsManagerController extends ControllerBase {

    protected $data;
    protected $routeMatch;
    protected $formBuilder;

    public function __construct(DatabaseService $data, RouteMatchInterface $routeMatch, FormBuilderInterface $formBuilder) {
        $this->data = $data;
        $this->routeMatch = $routeMatch;
        $this->formBuilder = $formBuilder;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('fashion_style_subscription_block.database'),
            $container->get('current_route_match'),
            $container->get('form_builder')
        );
    }

    public function content() {
        $result = $this->data->getAll();
        $template = '<table class="subscription"><tr><th>Email</th><th>Subscribed at</th><th>Operations</th><th></th></tr>';
        foreach ($result as $key => $item) {
            $message = '';
            if($item->updated == 1) {
                $message = "Updated";
            }
            $template .= '<tr><td>' . $item->email . '</td><td>' . date('m-d-Y', $item->subscribed_since) . '</td>';
            $template .= '<td><a href="/subscribe/' . $item->id . '/mail" class="use-ajax" data-dialog-type="modal">';
            $template .= 'SEND EMAIL</a></td><td>';
            $template .= '<div id="sub-updated-' . $item->id . '"><span>' . $message . '</span></div>';
            $template .= '</td></tr>';
        }
        $template .= '</table>';
        return [
            '#markup' => $template,
        ];
    }

    public function openMailForm() {
        $id = $this->routeMatch->getParameter('id');
        $email = $this->data->getEmailById($id);
        $form = $this->formBuilder->getForm('\Drupal\fashion_style_subscribe_block\Form\ModalMailForm', $email);
        return $form;
    }
}