<?php

namespace Drupal\fashion_style_subscribe_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Subscription' block.
 *
 * @Block(
 *   id = "subscription_block",
 *   admin_label = @Translation("Subscription Block"),
 *   category = @Translation("Blocks"),
 * )
 */

class SubscriptionBlock extends BlockBase {

    public function build()
    {
        $form = \Drupal::formBuilder()->getForm('\Drupal\fashion_style_subscribe_block\Form\SubscriptionForm');
        return $form;
    }
}