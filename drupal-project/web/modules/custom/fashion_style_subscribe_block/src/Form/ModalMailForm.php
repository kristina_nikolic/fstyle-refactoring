<?php

namespace Drupal\fashion_style_subscribe_block\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\State\StateInterface;
use Drupal\fashion_style_subscribe_block\Service\DatabaseService;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\fashion_style_subscribe_block\Service\MailingSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ModalMailForm extends FormBase {

    protected $data;
    protected $routeMatch;
    protected $logger;
    protected $config;
    protected $state;
    protected $mailing;



    public function __construct(DatabaseService $data, RouteMatchInterface $routeMatch, LoggerChannelFactory $logger,
                                ConfigFactory $config, StateInterface $state, MailingSettingsService $mailingSettingsService) {
        $this->data = $data;
        $this->routeMatch = $routeMatch;
        $this->logger = $logger;
        $this->config = $config;
        $this->state = $state;
        $this->mailing = $mailingSettingsService;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
        $container->get('fashion_style_subscription_block.database'),
        $container->get('current_route_match'),
        $container->get('logger.factory'),
        $container->get('config.factory'),
        $container->get('state'),
        $container->get('fashion_style_subscription_block.mailing')
        );
    }

    public function getFormId() {
        return 'modal_form_mail_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $email = NULL) {
        $form['#prefix'] = '<div id="modal_mail_form">';
        $form['#sufix'] = '</div>';

        // The status messages that will contain any form errors.
        $form['status_messages'] = [
            '#type' => 'status_messages',
            '#weight' => -10,
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Send mail to:'),
            '#value' => $email[0]->email,
            '#required' => TRUE,
            '#disabled' => TRUE,
        ];

        $form['subject'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Subject:'),
        ];

        $form['mail_body'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Mail Body:'),
            '#default_value' => $this->mailing->replaceTokens($this->state->get('mail_body')),
            '#required' => TRUE,
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Send mail'),
            '#button_type' => 'primary',
            '#attributes' => [
                'class' => ['use-ajax']
            ],
            '#ajax' => [
                'callback' => [
                    $this, 'submitSendMail'
                ],
                'event' => 'click',
            ],
        ];

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

        return $form;
    }

    public function submitSendMail(array $form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        $mailManager = new PhpMail();
        $header = $this->mailing->getMailHeader();
        $message = [];
        $message['headers'] = $header;
        $message['subject'] = $form_state->getValues()['subject'];
        $message['to'] = $form_state->getValues()['email'];
        $message['body'] = $form_state->getValues()['mail_body'];
        $result = $mailManager->mail($message);
        $to = $message['to'];
        if ($result) {
            $memo = t('Mail is sent to @email.', ['@email' => $to]);
            $this->logger('Subscriber')->info($memo);
        }
        $memo = t('Mail failed to send on: @email.', ['@email' => $to]);
        $this->logger('Subscriber')->notice($memo);

        // If there are any form errors, re-display the form.
        if ($form_state->hasAnyErrors()) {
            $response->addCommand(new ReplaceCommand('#modal_mail_form', $form));
        }
        else {
            $response->addCommand(new OpenModalDialogCommand("Success!", 'The modal form has been submitted.', ['width' => 800]));
        }

        $id = $this->routeMatch->getParameter('id');
        $updated = $this->data->setUpdated($id);
        if($updated){
            $text = '<span>Updated</span>';
        } else {
            $text = '<span>Updating failed</span>';
        }
        $response->addCommand(new ReplaceCommand('#sub-updated-'. $id, $text));
        return $response;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}