<?php

namespace Drupal\fashion_style_subscribe_block\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fashion_style_subscribe_block\Service\DatabaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;


class SubscriptionForm extends ConfigFormBase {

    protected $data;

    public function __construct(ConfigFactoryInterface $config_factory, DatabaseService $data)
   {
       parent::__construct($config_factory);
       $this->data = $data;
   }

   public static function create(ContainerInterface $container) {

       return new static(
           $container->get('config.factory'),
           $container->get('fashion_style_subscription_block.database')
       );
   }

    public function getFormId() {

        return 'subscription_form';
    }

    protected function getEditableConfigNames() {

        return ['fashion_style_subscription_block.settings'];
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Please subscribe to our Newsletter'),
            '#placeholder' => $this->t('Enter your email'),
            '#suffix' => '<div class="email-invalid"></div>',
            '#attributes' => [
                'class'=>['use-ajax']
            ],
            '#ajax' => [
                'callback' => [
                    $this, 'emailValidate'
                ],
                'effect' => 'fade',
                'event' => 'onchange',
            ],
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Subscribe'),
            '#button_type' => 'primary',
            '#attributes' => [
                'class'=>['use-ajax']
            ],
            '#ajax' => [
                'callback' => [
                    $this, 'submitCustomForm'
                ],
                'event' => 'click',
            ],
        ];
        return $form;
    }

    public function emailValidate(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        $text = '';
        if ($form_state->hasValue('email')) {
            $email = $form_state->getValue('email');
            if (!$this->data->validate($email)) {
                $text = '<span>Email is invalid or taken</span>';
            }
        }
        $response->addCommand(new HtmlCommand('.email-invalid', $text));
        return $response;
    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        if ($form_state->hasValue('email')) {
            $email = $form_state->getValue('email');
            if (!$this->data->validate($email)) {
                $form_state->setError($form['email']);
            }
        }
    }

    public function submitCustomForm(array &$form, FormStateInterface $form_state) {
        $response  = new AjaxResponse();
        $email = $form_state->getValue('email');
        if (empty($form_state->getErrors())) {
            $this->data->save($email);
            $text = '<span> You subscribed!</span>';
        } else {
            $text = '<span> You already subscribed!</span>';
        }
        $response->addCommand(new ReplaceCommand('#block-subscriptionblock', $text));
        return $response;
    }
}