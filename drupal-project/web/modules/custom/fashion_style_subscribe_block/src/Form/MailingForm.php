<?php

namespace Drupal\fashion_style_subscribe_block\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailingForm extends FormBase {

    protected $emailValidator;
    protected $state;

    public function __construct(EmailValidator $emailValidator, StateInterface $state) {
        $this->emailValidator = $emailValidator;
        $this->state = $state;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('email.validator'),
            $container->get('state')
        );
    }

    public function getFormId() {
        return 'mailing_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $email = NULL) {
        $form['#prefix'] = '<div id="mailing_form">';
        $form['#sufix'] = '</div>';

        $form['sender_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Sender name:'),
            '#required' => TRUE,
            '#default_value' => $this->state->get('sender_name') ? $this->state->get('sender_name') : '',
        ];

        $form['sender_mail'] = [
            '#type' => 'email',
            '#title' => $this->t('Sender email:'),
            '#required' => TRUE,
            '#default_value' => $this->state->get('sender_mail') ? $this->state->get('sender_mail') : '',
        ];

        $form['cc_mail'] = [
            '#type' => 'email',
            '#title' => $this->t('cc:'),
            '#default_value' => $this->state->get('cc_mail') ? $this->state->get('cc_mail') : '',
        ];

        $form['mail_body'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Mail body:'),
            '#required' => TRUE,
            '#default_value' => $this->state->get('mail_body') ? $this->state->get('mail_body') : '',
            '#description' => 'Available tokens: @siteName = Name of site, @userRole = Role of current user.',
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
            '#attributes' => [
                'class' => ['use-ajax']
            ],
            '#ajax' => [
                'callback' => [
                    $this, 'submitForm'
                ],
                'event' => 'click',
            ],
        ];

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if($form_state->hasValue('sender_mail')) {
            $email = $form_state->getValue('sender_mail');
            if(!$this->emailValidator->isValid($email)) {
                $form_state->setErrorByName('sender_mail', t('The email is invalid.'));
            }
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        if(!$form_state->getErrors()) {
            $data = [
                'sender_name' => $form_state->getValue('sender_name'),
                'sender_mail' => $form_state->getValue('sender_mail'),
                'cc_mail' => $form_state->getValue('cc_mail'),
                'mail_body' => $form_state->getValue('mail_body'),
            ];
            $this->state->setMultiple($data);
            $response->addCommand(new ReplaceCommand('.messages--status', 'Your settings are saved.'));
        }
        return $response;
    }
}