<?php

namespace Drupal\fashion_style_subscribe_block\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\RouteMatchInterface;

class DatabaseService {

    protected $current_time;
    protected $database;
    protected $routeMatch;

    public function __construct(TimeInterface $current_time, Connection $database, RouteMatchInterface $routeMatch) {
        $this->current_time = $current_time;
        $this->database = $database;
        $this->routeMatch = $routeMatch;
    }

    public function validate($email) {
        $result = $this->database->query("SELECT * FROM {subscribed} WHERE email = :email", [':email' => $email])->fetchAll();
        if(empty($result)) {
            if(filter_var($email, FILTER_VALIDATE_EMAIL))
               return true;
        }
        return false;
    }

    public function getAll() {
        $result = $this->database->query("SELECT * FROM {subscribed}")->fetchAll();
        return $result;
    }

    public function getEmailById($id) {
        $email = $this->database->query("SELECT email FROM {subscribed} WHERE id = :id", [':id' => $id])->fetchAll();
        return $email;
    }

    public function save($email) {
        $time = $this->current_time->getCurrentTime();
        try {
            $result = $this->database->insert('subscribed')
                ->fields([
                    'email' => $email,
                    'subscribed_since' => $time,
                ])->execute();
        } catch (\Exception $e) {
        }
        return $result;
    }

    public function setUpdated($id) {
        $routeId = $this->routeMatch->getParameter('id');
        if($routeId == $id) {
            $sub = $this->database->query("SELECT updated FROM {subscribed} WHERE id = :id", [':id' => $id])->fetchField();
            if($sub == 0) {
                $this->database->update('subscribed')->fields(['updated' => 1])->condition('id', $id, '=')->execute();
                $result = true;
            } else {
                $result = false;
            }
        }
        return $result;
    }
}