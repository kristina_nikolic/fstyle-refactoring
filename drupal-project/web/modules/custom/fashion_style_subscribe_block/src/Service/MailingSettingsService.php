<?php

namespace Drupal\fashion_style_subscribe_block\Service;

use Drupal\Core\Session\AccountProxy;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Psr\Container\ContainerInterface;

class MailingSettingsService {

    protected $currentUser;
    protected $state;

    public function __construct(AccountProxy $current_user, StateInterface $state) {
        $this->currentUser = $current_user;
        $this->state = $state;
    }

    public function create(ContainerInterface $container) {
        return new static(
            $container->get('current_user'),
            $container->get('state')
        );
    }

    public function getMailHeader() {
        $from = $this->state->get('sender_mail');
        $sender = $this->state->get('sender_name');
        $cc = $this->state->get('cc_mail');
        $header = [
            'content-type' => 'text/html',
            'MIME-Version' => '1.0',
            'from' =>  $sender . ' <' . $from . '>',
            'cc' => $cc ? $cc : '',
            'Return-Path' => $from,
        ];
        return $header;
    }

    public function replaceTokens() {
        $siteName = $this->state->get('sender_name');
        $tokenizedMessage = $this->state->get('mail_body');

        $message = new TranslatableMarkup($tokenizedMessage, [
            '@siteName' =>$siteName,
            '@userRole' => $this->getUserRole($this->currentUser->getRoles()),
        ]);

        $mailBody = $message->render();

        return $mailBody;
    }

    private function getUserRole($roles) {
        foreach ($roles as $role) {
            if($role != 'authenticated' && ($role != 'anonymous')) {
                return $role;
            }
        }
    }

}